// assignment operators
// Basic Assignment Operator (=)
// The assignment operator adds the value of the right operand to a variable and assigns the result to the variable
let assignmentNumber = 8;
console.log(assignmentNumber);

//addition assignment operator
totalNumber = assignmentNumber + 2;
console.log("result of addition =" + totalNumber);

//arithmetic operator (+, -, *, / , %)
let x = 2;
let y = 5;

let sum = x + y;
console.log("result of addition " + sum)


// multiple operators and parentheses
let mdas = 1 + 2 - 3 * 4 / 5;
console.log(mdas);

let juan = "juan";
console.log(1 == 1);
console.log(1 == 2);
console.log(1 == '1');
console.log(0 == false);
//Compares two strings that are the same 
console.log('juan' == 'juan');
//Compares a string with the variable "juan" declared above 
console.log('juan' == juan);


console.log(1 != 1);
console.log(1 != 2);
console.log(1 != '1');
console.log(0 != false);
console.log('juan' != 'juan');
console.log('juan' != juan);


//Some comparison operators check whether one value is greater or less than to the other value.

let a = 50;
let b = 65;

//GT or Greater Than operator ( > )
let isGreaterThan = a > b; // 50 > 65
//LT or Less Than operator ( < )
let isLessThan = a < b; // 50 < 65
//GTE or Greater Than Or Equal operator ( >= ) 
let isGTorEqual = a >= b; // 50 >= 65
//LTE or Less Than Or Equal operator ( <= ) 
let isLTorEqual = a <= b; // 50 <= 65

//Like our equality comparison operators, relational operators also return boolean which we can save in a variable or use in a conditional statement.
console.log(isGreaterThan);
console.log(isLessThan);
console.log(isGTorEqual);
console.log(isLTorEqual);

// Logical Or Operator (|| - Double Pipe)
// Returns true if one of the operands are true 

// 1 || 1 = true
// 1 || 0 = true
// 0 || 1 = true
// 0 || 0 = false

let someRequirementsMet = isLegalAge || isRegistered;
console.log("Result of logical OR Operator: " + someRequirementsMet);

// Logical Not Operator (! - Exclamation Point)
// Returns the opposite value 
let someRequirementsNotMet = !isRegistered;
console.log("Result of logical NOT Operator: " + someRequirementsNotMet);